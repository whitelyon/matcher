# Matcher

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.


### Prerequisites

Before execute the application you must install the following

```
* Java 1.8
* Maven 4 or Higher
```

### Running the application

Go to "build" folder and run: 



```
java -jar recruiting.jar 
```