package mapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vividseats.model.Event;
import com.vividseats.model.EventType;

public class EventMapper {

	public final static String EVENT_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

	/**
	 * Map a event line to a Event Model Object
	 * 
	 * @param type
	 * @param eventLine
	 * @return
	 * @throws ParseException
	 */
	public static Event arrayToEvent(EventType type, String[] eventLine) throws ParseException {
		Event event = null;

		Date date = null;

		switch (type) {
		case EXISTING:
			date = EventMapper.getDate(eventLine[3] + " " + eventLine[4], EVENT_DATE_PATTERN);
			event = new Event(eventLine[0], eventLine[1], eventLine[2], date);

			break;
		case INCOMING:
			date = EventMapper.getDate(eventLine[3], EVENT_DATE_PATTERN);
			event = new Event(eventLine[4], eventLine[1], eventLine[2], date);

			break;
		}

		return event;
	}

	/**
	 * Get date from text using a pattern
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static Date getDate(String date, String pattern) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return simpleDateFormat.parse(date);
	}

	/**
	 * Map a list of events
	 * 
	 * @param type
	 * @param eventList
	 * @return
	 */
	public static Set<Event> getEventList(EventType type, List<String[]> eventList) {
		Set<Event> events = new HashSet<Event>();

		// Remove Header
		eventList.remove(0);

		Event event = null;

		for (String[] eventArray : eventList) {
			try {
				event = arrayToEvent(type, eventArray);
				events.add(event);
			} catch (ParseException e) {
				System.out.println(e.getMessage() + " : Event Line: " + getEventLineModel(event));
			}
		}
		return events;
	}

	/**
	 * Logging
	 * 
	 * @param eventLine
	 * @return
	 */
	public static String getEventLineArray(String[] eventLine) {
		StringBuilder builder = new StringBuilder();
		for (String value : eventLine) {
			builder.append("[").append(value).append("]").append("::");
		}
		return builder.toString();
	}

	/**
	 * Logging
	 * 
	 * @param eventLine
	 * @return
	 */
	public static String getEventLineModel(Event eventLine) {
		StringBuilder builder = new StringBuilder();
		builder.append("[").append(eventLine.getName()).append("]").append("::");
		builder.append("[").append(eventLine.getVenue()).append("]").append("::");
		builder.append("[").append(eventLine.getDate()).append("]").append("::");
		return builder.toString();
	}
}
