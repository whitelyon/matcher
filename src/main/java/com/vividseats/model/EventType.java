package com.vividseats.model;

public enum EventType {
	EXISTING, INCOMING
}
