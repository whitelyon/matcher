package com.vividseats;

import java.util.Set;

import com.vividseats.model.Event;
import com.vividseats.model.EventType;
import com.vividseats.service.EventService;
import com.vividseats.service.MatcherService;

/**
 * <p>
 * Given a list of new Events (incoming.csv), this class uses the
 * {@link MatcherService#match(java.util.List) } method to match existing events
 * and reports how many incoming events where:
 * </p>
 *
 * <ul>
 * <li>matched</li>
 * <li>correctly matched</li>
 * <li>unmatched</li>
 * </ul>
 *
 * <p>
 * Please modify this code to increase the number of correct matches. Feel free
 * to refactor, unit test and apply other software engineering practices as you
 * see fit.
 * </p>
 *
 *
 */
public class Main {

	public static void main(String[] args) throws Exception {
		Set<Event> existing = EventService.getInstance().getAllEvents(EventType.EXISTING);
		Set<Event> incoming = EventService.getInstance().getAllEvents(EventType.INCOMING);

		Set<Event> matches = MatcherService.getInstance().match(existing, incoming);

		int correctMatches = MatcherService.getInstance().verify(existing, matches);

		System.out.println("MATCHES: " + matches.size());
		System.out.println("Correctly Matched: " + correctMatches);
		System.out.println("Unmatched:  " + (incoming.size() - matches.size()));
	}

}
