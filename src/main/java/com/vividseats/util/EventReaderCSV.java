package com.vividseats.util;

import java.util.Set;

import com.vividseats.model.Event;

public interface EventReaderCSV {

	/**
	 * Read a CSV file
	 * @return
	 * @throws Exception
	 */
	Set<Event> getAllEvents() throws Exception;
}
