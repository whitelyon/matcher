package com.vividseats.util.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Set;

import com.vividseats.model.Event;
import com.vividseats.model.EventType;
import com.vividseats.util.EventReaderCSV;

import au.com.bytecode.opencsv.CSVReader;
import mapper.EventMapper;

public class IncomingEventReaderImpl implements EventReaderCSV {

	private static final String FILE_NAME = "/incoming.csv";

	@Override
	public final Set<Event> getAllEvents() throws Exception {
		CSVReader reader = new CSVReader(new BufferedReader(
				new InputStreamReader(ExistingEventReaderImpl.class.getResourceAsStream(FILE_NAME))));

		return EventMapper.getEventList(EventType.INCOMING, reader.readAll());
	}

}
