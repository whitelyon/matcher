package com.vividseats.service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.vividseats.model.Event;

public class MatcherService {

    private static final MatcherService instance = new MatcherService();

    public static final MatcherService getInstance() {
        return instance;
    }

    public static final int EVENT_NAME = 1;
    
    /**
     * <p>
     * This method matches the events in the {@code incoming} list.</p>
     * <p>
     * @param existing
     * @param incoming
     * @return
     */
    public final Set<Event> match(Set<Event> existing, Set<Event> incoming) {
		Set<Event> matches = new HashSet<Event>();

		for (Event event : incoming) {
			if (existing.contains(event)) {
				matches.add(event);
			}
		}
		
		return matches;
    }
    
    /**
     * Verify the matches vs existing by PID
     * @param existing
     * @param matches
     * @return
     */
    public final int verify(Set<Event> existing, Set<Event> matches) {
    	Set<Event> existingCopy = existing.stream().collect(Collectors.toSet());
    	
    	existingCopy.retainAll(matches);
    	
    	int correctMatch = 0;
    	
    	for (Event match : matches) {
    		for (Event event : existingCopy) {
    			if (match.equals(event) && match.getPid().compareToIgnoreCase(event.getPid()) == 0) {
    				correctMatch++;
    			}
    		}
    	}
    	
    	return correctMatch;
    }
}
