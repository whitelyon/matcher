package com.vividseats.service;

import java.util.Set;

import com.vividseats.model.Event;
import com.vividseats.model.EventType;
import com.vividseats.util.EventReaderCSV;
import com.vividseats.util.impl.ExistingEventReaderImpl;
import com.vividseats.util.impl.IncomingEventReaderImpl;

public class EventService {
    private static final EventService instance = new EventService();
    public static final EventService getInstance() { return instance;}

    /**
     * Returns a list of events.
     * @param type
     * @return
     * @throws Exception
     */
    public final Set<Event> getAllEvents(EventType type) throws Exception {
    	
    	EventReaderCSV reader = null;
    	
    	switch (type) {
		case EXISTING:
			reader = new ExistingEventReaderImpl();
			break;
		case INCOMING:
			reader = new IncomingEventReaderImpl();
			break;
		}
    	
        return reader.getAllEvents();
    }
}
