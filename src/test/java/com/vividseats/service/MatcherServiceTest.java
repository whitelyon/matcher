package com.vividseats.service;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.vividseats.model.Event;

import mapper.EventMapper;

import static org.mockito.Mockito.*;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class MatcherServiceTest {
	@InjectMocks
	private MatcherService matcherService;

	@Before
	public void setUp() {
		matcherService = mock(MatcherService.class);

	}

	@Test
	public void matcherTest1() throws ParseException {
		Set<Event> existing = getExistingEvents();
		Set<Event> incoming = getIncomingEventsTest1();

		Set<Event> matches = matcherService.match(existing, incoming);
		
		int correctMatches = matcherService.verify(existing, matches);
		
		assertTrue(correctMatches == 3);
		assertTrue((existing.size() - matches.size()) == 1);
	}

	/**
	 * Get a list of existing events
	 * 
	 * @return
	 * @throws ParseException
	 */
	private Set<Event> getExistingEvents() throws ParseException {
		Set<Event> events = new HashSet<Event>();

		Date date1 = EventMapper.getDate("2014-01-31 20:00:00", EventMapper.EVENT_DATE_PATTERN);
		Date date2 = EventMapper.getDate("2014-01-30 20:00:00", EventMapper.EVENT_DATE_PATTERN);
		Date date3 = EventMapper.getDate("2014-01-29 20:00:00", EventMapper.EVENT_DATE_PATTERN);
		Date date4 = EventMapper.getDate("2014-01-28 20:00:00", EventMapper.EVENT_DATE_PATTERN);

		Event event1 = new Event("1", "Event Test 1", "Venue Test 1", date1);
		Event event2 = new Event("2", "Event Test 2", "Venue Test 2", date2);
		Event event3 = new Event("3", "Event Test 3", "Venue Test 3", date3);
		Event event4 = new Event("4", "Event Test 4", "Venue Test 4", date4);

		events.add(event1);
		events.add(event2);
		events.add(event3);
		events.add(event4);

		return events;
	}

	private Set<Event> getIncomingEventsTest1() throws ParseException {
		Set<Event> events = new HashSet<Event>();

		Date date1 = EventMapper.getDate("2014-02-31 14:00:00", EventMapper.EVENT_DATE_PATTERN);
		Date date2 = EventMapper.getDate("2014-01-30 20:00:00", EventMapper.EVENT_DATE_PATTERN);
		Date date3 = EventMapper.getDate("2014-01-29 20:00:00", EventMapper.EVENT_DATE_PATTERN);
		Date date4 = EventMapper.getDate("2014-01-28 20:00:00", EventMapper.EVENT_DATE_PATTERN);

		Event event1 = new Event("11", "New Event Test 1", "Venue Test 1", date1);
		Event event2 = new Event("2", "Event Test 2", "Venue Test 2", date2);
		Event event3 = new Event("3", "Event Test 3", "Venue Test 3", date3);
		Event event4 = new Event("4", "Event Test 4", "Venue Test 4", date4);

		events.add(event1);
		events.add(event2);
		events.add(event3);
		events.add(event4);

		return events;
	}
}
